from django.urls import path
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from scout.views import (
    DiscoverView, DiscoverCardView, FoodDetailView, FoodFilterView, FoodListView,
    StudyDetailView, StudyFilterView, StudyListView, TechDetailView, TechFilterView,
    TechListView, spot_image_view, item_image_view
)


urlpatterns = [
    path('', RedirectView.as_view(url='/madison', permanent=True)),
    path('<str:campus>/', DiscoverView.as_view(), {"template_name": "scout/discover.html"}),
    path(
        '<str:campus>/discover_card/<str:discover_category>/',
        DiscoverCardView.as_view(),
        {"template_name": "scout/discover_card.html"}
    ),
    path('<str:campus>/food/', FoodListView.as_view(), {"template_name": "scout/food/list.html"}),
    path(
        '<str:campus>/food/<int:spot_id>/',
        FoodDetailView.as_view(),
        {"template_name": "scout/food/detail.html"}
    ),
    path(
        '<str:campus>/food/filter/',
        FoodFilterView.as_view(),
        {"template_name": "scout/food/filter.html"}
    ),
    path(
        '<str:campus>/study/',
        StudyListView.as_view(),
        {"template_name": "scout/study/list.html"}
    ),
    path('<str:campus>/study/<int:spot_id>/',
        StudyDetailView.as_view(),
        {"template_name": "scout/study/detail.html"}
    ),
    path('<str:campus>/study/filter/',
        StudyFilterView.as_view(),
        {"template_name": "scout/study/filter.html"}
    ),
    path('<str:campus>/tech/', TechListView.as_view(), {"template_name": "scout/tech/list.html"}),
    path(
        '<str:campus>/tech/<int:item_id>/',
        TechDetailView.as_view(),
        {"template_name": "scout/tech/detail.html"}
    ),
    path(
        '<str:campus>/tech/filter/',
        TechFilterView.as_view(),
        {"template_name": "scout/tech/filter.html"}
    ),
    path('h/', RedirectView.as_view(url='/h/seattle')),
    path('h/<str:campus>/', DiscoverView.as_view(), {"template_name": "hybridize/discover.html"}),
    path(
        'h/<str:campus>/discover_card/<str:discover_category>/',
        DiscoverCardView.as_view(),
        {"template_name": "hybridize/discover_card.html"}
    ),
    path(
        'h/<str:campus>/food/',
        FoodListView.as_view(),
        {
            "template_name": "hybridize/food/list.html",
            "app_type": "food"
        }
    ),
    path('h/<str:campus>/food/<int:spot_id>/',
        FoodDetailView.as_view(),
        {"template_name": "hybridize/food/detail.html"}
    ),
    path('h/<str:campus>/food/filter/',
        FoodFilterView.as_view(),
        {"template_name": "hybridize/food/filter.html"}
    ),
    path('h/<str:campus>/study/',
        StudyListView.as_view(),
        {
            "template_name": "hybridize/study/list.html",
            "app_type": "study"
        }
    ),
    path('h/<str:campus>/study/<int:spot_id>/',
        StudyDetailView.as_view(),
        {"template_name": "hybridize/study/detail.html"}
    ),
    path('h/<str:campus>/study/filter/',
        StudyFilterView.as_view(),
        {"template_name": "hybridize/study/filter.html"}
    ),
    path('h/<str:campus>/tech/',
        TechListView.as_view(),
        {
            "template_name": "hybridize/tech/list.html",
            "app_type": "tech"
        }
    ),
    path('h/<str:campus>/tech/<int:item_id>/',
        TechDetailView.as_view(),
        {"template_name": "hybridize/tech/detail.html"}
    ),
    path('h/<str:campus>/tech/filter/',
        TechFilterView.as_view(),
        {"template_name": "hybridize/tech/filter.html"}
    ),
    path('images/<int:spot_id>/image/<int:image_id>/', spot_image_view),
    path('item/images/<int:item_id>/image/<int:image_id>/', item_image_view),
]
