DoIT fork of [scout client app](https://github.com/uw-it-aca/scout)
by the University of Washington Information Technology's Academic Experience Design & Delivery group.
Major changes are from upstream project is this fork has been updated to Python 3.x and Django 2.x.

### Requirement
* Python 3.6.x
* Django 2.x

### Development and Configuration
Install scout as a Django module in [WiScout](https://git.doit.wisc.edu/adi-ia/wiscout) Django app.

* Clone WiScout to your machine.

* Install scout as an editable dependency.
  
```sh
# from wiscout directory
$ pipenv install -e /path/to/scout
```
  
* Add `scout` and `compressor` to `INSTALLED_APPS`
  
```py
# wiscout/settings.py
INSTALLED_APPS = [
    ...
    'scout',
    'compressor',
    ...
]
```

* Add `django_mobileesp` to MIDDLEWARE
```py
# wiscout/settings.py

from django_mobileesp.detector import python_agent as agent

MIDDLEWARE = [
    ...
    'django_mobileesp.middleware.UserAgentDetectionMiddleware',
    ...
]
```

* Add additional context_processors to `TEMPLATES-OPTIONS`
```py
# wiscout/settings.py
TEMPLATES = [ 
    {   
        ...
        'OPTIONS': {
            'context_processors': [
                ...
                'scout.context_processors.google_maps',
                'scout.context_processors.google_analytics',
                'scout.context_processors.is_desktop',
                'scout.context_processors.is_hybrid',
                ...
            ],  
        }, 
        ... 
    },  
]
```

* Add `STATICFILES_FINDERS`
```py
# wiscout/settings.py
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
]
```

* Add `STATIC_ROOT` 
```py
# wiscout/settings.py
STATIC_ROOT = 'static/'
```

* Add Compressor options for Sass compilation and CSS/JavaScript minification
```py
COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'sass --scss {infile} {outfile}'),
    ('text/x-scss', 'django_pyscss.compressor.DjangoScssFilter')
)

COMPRESS_ENABLED = True
COMPRESS_CSS_FILTERS = [
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter'
]
COMPRESS_JS_FILTERS = [
    'compressor.filters.jsmin.JSMinFilter',
]

COMPRESS_ROOT = 'static'
```

* Add campus list
```py
CAMPUS_URL_LIST = ['seattle', 'tacoma', 'bothell']
```

* Add the `scout` app to your project's `urls.py`
```py
# wiscout/settings.py
handler404 = 'scout.views.custom_404_response'

urlpatterns = [
    path('api/', include('spotseeker_server.urls')),
    path('', include('scout.urls')),
]
```

* Configure remote `spotseeker_server` location
```py
SPOTSEEKER_HOST = 'http://127.0.0.1:8000' # for locally running instance
SPOTSEEKER_OAUTH_KEY = ''
SPOTSEEKER_OAUTH_SECRET = ''
SPOTSEEKER_DAO_CLASS = 'spotseeker_restclient.dao_implementation.spotseeker.Live'
```
There is a mock implementation for testing purposes which can be configured using:
```py
SPOTSEEKER_DAO_CLASS = 'spotseeker_restclient.dao_implementation.spotseeker.File'
```

* Configuring Google Map API (see [here](https://developers.google.com/maps/documentation/javascript/get-api-key) to obtain a key) and Google Analytics keys
```py
# wiscout/settings.py
GOOGLE_MAPS_API='<Google MAP API key>'
GOOGLE_ANALYTICS_KEY='<Google Analytics key>'
```

### Run any migrations and run the server
```py
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py runserver
```

Now point your browser to - [http://127.0.0.1:8000/](http://127.0.0.1:8000/).

### Run Unit Tests
```py
# from wiscout 
$ python manage.py test scout.test
```

### Open items
* Mobile/user agent supports - `django_mobileesp.middleware.UserAgentDetectionMiddleware` (`scout.context_processors.is_desktop`)
* Authentication/Authorization.
* Fix failing tests.