import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


def package_files(dir):
    """Get file paths of all files in this directory and its children.

    This recursively walk the directory structure of `dir` and returns the list
    of files in the directory and all of its subdirectories.

    Args:
        dir (str): directory path relatively to `setup.py`

    Returns:
        list: relative file paths in every subdirectory of `dir`
    """
    paths = []
    for (path, _, filenames) in os.walk(dir):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
    return paths


data = []
data += package_files('scout/static')
data += package_files('scout/templates')


setup(
    name='scout',
    version='0.0.6',
    packages=find_packages(),
    package_data={'': data},
    include_package_data=True,
    install_requires = [
        'setuptools',
        'django',
        'django_compressor',
        'pytz',
        'beautifulsoup4',
        'html5lib==0.9999999',
        'requests',
        'django-pyscss',
        'django-user-agents'
    ],
    license='Apache License, Version 2.0',  # example license
    description='A Django app for finding resources on campus.',
    long_description=README,
)
